﻿<%@ Page Title="Localizacao Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Localizacao.aspx.cs" Inherits="SmartCity.Localizacao" %>


<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Content/Site.css" rel="stylesheet" />
        <div>
     <div align="center"><h1>UTAD</h1>
            </div>
            <div align="center">
                <h2>Últimas 5 leituras</h2></div>
        </div>
        <br />
        <p>
        <asp:Chart ID="Chart1" runat="server" OnLoad="Chart1_Load" Width="387px" Height="245px">
                <series>
                    <asp:Series Name="Series1">
                    </asp:Series>
                </series>
                <chartareas>
                    <asp:ChartArea Name="ChartArea1">
                         <AxisY Title="°C"></AxisY>
                        <AxisX Title="Leituras" IsLabelAutoFit="True"><LabelStyle Angle="-90" Interval="1" /></AxisX>
                    </asp:ChartArea>
                </chartareas>
            </asp:Chart>
            <asp:Chart ID="Chart2" runat="server" Width="387px" OnLoad="Chart2_Load" Height="245px">
                <series>
                    <asp:Series ChartArea="ChartArea1" Name="Series1" YValuesPerPoint="6">
                    </asp:Series>
                </series>
                <chartareas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisY Title="kg/m³"></AxisY>
                        <AxisX Title="Leituras" IsLabelAutoFit="True"><LabelStyle Angle="-90" Interval="1" /></AxisX>
                    </asp:ChartArea>
                </chartareas>
            </asp:Chart>

            <asp:Chart ID="Chart3" runat="server" Height="245px" OnLoad="Chart3_Load" Width="387px">
                <Series>
                    <asp:Series Name="Series1">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisY Title="cd"></AxisY>
                        <AxisX Title="Leituras" IsLabelAutoFit="True"><LabelStyle Angle="-90" Interval="1" /></AxisX>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </p>
        <div align="center">
                 <br />
        <br />
        <br />
            <h2>Mensal</h2>
        </div>
             <br />
           <p>
            <asp:Chart ID="Chart4" runat="server" Height="245px" OnLoad="Chart4_Load" Width="387px">
                <Series>
                    <asp:Series Name="Series1">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisY Title="°C"></AxisY>
                        <AxisX Title="Meses" IsLabelAutoFit="True"><LabelStyle Angle="-90" Interval="1" /></AxisX>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
            <asp:Chart ID="Chart5" runat="server" Height="245px" OnLoad="Chart5_Load" Width="387px">
                <Series>
                    <asp:Series Name="Series1">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisY Title="kg/m³"></AxisY>
                        <AxisX Title="Meses" IsLabelAutoFit="True"><LabelStyle Angle="-90" Interval="1" /></AxisX>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
            <asp:Chart ID="Chart6" runat="server" Height="245px" OnLoad="Chart6_Load" Width="387px">
                <Series>
                    <asp:Series Name="Series1">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisY Title="cd"></AxisY>
                        <AxisX Title="Meses" IsLabelAutoFit="True"><LabelStyle Angle="-90" Interval="1" /></AxisX>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
               <br />
               <br />
               <br />
               <br />
        </p>
        <div align="center">
            <h2>Anual</h2>
        </div>
        <br />
             <p>
                 <asp:Chart ID="Chart7" runat="server" Height="245px" Width="387px" OnLoad="Chart7_Load">
                     <Series>
                         <asp:Series ChartType="Spline" Name="Series1">
                         </asp:Series>
                     </Series>
                     <ChartAreas>
                         <asp:ChartArea Name="ChartArea1">
                              <AxisY Title="°C"></AxisY>
                        <AxisX Title="Meses" IsLabelAutoFit="True"><LabelStyle Angle="-90" Interval="1" /></AxisX>
                         </asp:ChartArea>
                     </ChartAreas>
                 </asp:Chart>
                 <asp:Chart ID="Chart8" runat="server" Height="245px" Width="387px" OnLoad="Chart8_Load">
                     <Series>
                         <asp:Series ChartType="Spline" Name="Series1">
                         </asp:Series>
                     </Series>
                     <ChartAreas>
                         <asp:ChartArea Name="ChartArea1">
                         <AxisY Title="kg/m³"></AxisY>
                        <AxisX Title="Meses" IsLabelAutoFit="True"><LabelStyle Angle="-90" Interval="1" /></AxisX>
                         </asp:ChartArea>
                     </ChartAreas>
                 </asp:Chart>
<%--                 <div align="center">--%>
                 <asp:Chart ID="Chart9" runat="server" Height="245px" Width="387px" OnLoad="Chart9_Load">
                     <Series>
                         <asp:Series ChartType="Spline" Name="Series1">
                         </asp:Series>
                     </Series>
                     <ChartAreas>
                         <asp:ChartArea Name="ChartArea1">
                        <AxisY Title="cd"></AxisY>
                        <AxisX Title="Meses" IsLabelAutoFit="True"><LabelStyle Angle="-90" Interval="1" /></AxisX>
                         </asp:ChartArea>
                     </ChartAreas>
                 </asp:Chart>
<%--                     </div>--%>
        </p>
   </asp:Content>