﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartCity
{
    public class leituras
    {
        int id_leituras;
        string sensor_humidade;
        string sensor_temperatura;
        string sensor_luminosidade;
        DateTime data;
        int id_arduino;
        int id_localizacao;

        public int Id_leituras { get => id_leituras; set => id_leituras = value; }
        public string Sensor_humidade { get => sensor_humidade; set => sensor_humidade = value; }
        public string Sensor_temperatura { get => sensor_temperatura; set => sensor_temperatura = value; }
        public string Sensor_luminosidade { get => sensor_luminosidade; set => sensor_luminosidade = value; }
        public DateTime Data { get => data; set => data = value; }
        public int Id_arduino { get => id_arduino; set => id_arduino = value; }
        public int Id_localizacao { get => id_localizacao; set => id_localizacao = value; }
    }
}