﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartCity
{
    public class arduino
    {
        int id_arduino;
        int cod;
        DateTime data_instalacao;
        Boolean is_enabled;
        int id_localizacao;

        public int Id_arduino { get => id_arduino; set => id_arduino = value; }
        public int Cod { get => cod; set => cod = value; }
        public DateTime Data_instalacao { get => data_instalacao; set => data_instalacao = value; }
        public bool Is_enabled { get => is_enabled; set => is_enabled = value; }
        public int Id_localizacao { get => id_localizacao; set => id_localizacao = value; }
    }
}