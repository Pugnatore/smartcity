﻿<%@ Page Title="Distriro Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Distrito.aspx.cs" Inherits="SmartCity.Distrito" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Content/Site.css" rel="stylesheet" />
        <div>
     <div align="center"><h1>Vila Real</h1>
            </div>
            <div align="center"><h2>Mensal</h2></div>
        </div>
             <br />
           <p>
            <asp:Chart ID="Chart4" runat="server" Height="245px" OnLoad="Chart4_Load" Width="387px">
                <Series>
                    <asp:Series Name="Series1">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisY Title="°C"></AxisY>
                        <AxisX Title="Meses" IsLabelAutoFit="True"><LabelStyle Angle="-90" Interval="1" /></AxisX>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
            <asp:Chart ID="Chart5" runat="server" Height="245px" OnLoad="Chart5_Load" Width="387px">
                <Series>
                    <asp:Series Name="Series1">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisY Title="kg/m³"></AxisY>
                        <AxisX Title="Meses" IsLabelAutoFit="True"><LabelStyle Angle="-90" Interval="1" /></AxisX>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
            <asp:Chart ID="Chart6" runat="server" Height="245px" OnLoad="Chart6_Load" Width="387px">
                <Series>
                    <asp:Series Name="Series1">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisY Title="cd"></AxisY>
                        <AxisX Title="Meses" IsLabelAutoFit="True"><LabelStyle Angle="-90" Interval="1" /></AxisX>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
               <br />
               <br />
               <br />
               <br />
        </p>
        <div align="center">
            <h2>Anual</h2>
        </div>
        <br />
             <p>
                 <asp:Chart ID="Chart7" runat="server" Height="245px" Width="387px" OnLoad="Chart7_Load">
                     <Series>
                         <asp:Series ChartType="Spline" Name="Series1">
                         </asp:Series>
                     </Series>
                     <ChartAreas>
                         <asp:ChartArea Name="ChartArea1">
                              <AxisY Title="°C"></AxisY>
                        <AxisX Title="Meses" IsLabelAutoFit="True"><LabelStyle Angle="-90" Interval="1" /></AxisX>
                         </asp:ChartArea>
                     </ChartAreas>
                 </asp:Chart>
                 <asp:Chart ID="Chart8" runat="server" Height="245px" Width="387px" OnLoad="Chart8_Load">
                     <Series>
                         <asp:Series ChartType="Spline" Name="Series1">
                         </asp:Series>
                     </Series>
                     <ChartAreas>
                         <asp:ChartArea Name="ChartArea1">
                         <AxisY Title="kg/m³"></AxisY>
                        <AxisX Title="Meses" IsLabelAutoFit="True"><LabelStyle Angle="-90" Interval="1" /></AxisX>
                         </asp:ChartArea>
                     </ChartAreas>
                 </asp:Chart>
<%--                 <div align="center">--%>
                 <asp:Chart ID="Chart9" runat="server" Height="245px" Width="387px" OnLoad="Chart9_Load">
                     <Series>
                         <asp:Series ChartType="Spline" Name="Series1">
                         </asp:Series>
                     </Series>
                     <ChartAreas>
                         <asp:ChartArea Name="ChartArea1">
                        <AxisY Title="cd"></AxisY>
                        <AxisX Title="Meses" IsLabelAutoFit="True"><LabelStyle Angle="-90" Interval="1" /></AxisX>
                         </asp:ChartArea>
                     </ChartAreas>
                 </asp:Chart>
<%--                     </div>--%>
        </p>
   </asp:Content>
