﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SmartCity._Default" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Content/Site.css" rel="stylesheet" />
    <img src="Content/Index.PNG"  style="width: 1350px;margin-left: -100px;margin-top:  -20px;height: 500px;" />
    <div class="jumbotron" style="margin-left: -100px; width: 1350px;margin-top:  -50px;">
      <br />
        <div align="center">
        <h2 >Localização dos arduinos</h2>
        <p class="lead">No mapa embaixo é possivel observar a localização dos diferentes arduinos.</p>
        </div>
          <p>
             <div align="center">
    <iframe src="https://www.google.com/maps/d/embed?mid=1H--Fv47MD_Kp0UBmPqHBmaFq8FBMEmyW" width="1000" height="600"></iframe>
    </div>
           
        </p>
    </div>
    <div>
     <div align="center"><h1>Arduinos</h1></div>
        <br />
    <div class="container">
        <div class="row">
             <div class="container">
        <div class="row">
                   <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail img-thumb-bg" style="background-image: url('http://sp-instrumedica.pt/wp-content/uploads/2017/09/UTAD.png'); background-size: 400px; border-radius: 25px ">
                        <div class="overlay"></div>
                        <div class="caption">
                            <div class="tag"><a href="Stats.aspx?id=1">UTAD</a></div>
                            <div class="title"><a href="Stats.aspx?id=1">Arduino 1</a></div>
                            <div class="content">
                                <p>Arduino implementado na UTAD (Arduino 1)</p>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
     </div>
    </div>
        <br />
             <div align="center"><h1>Localização</h1></div>
        <br />
    <div class="container">
        <div class="row">
             <div class="container">
        <div class="row">
                   <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail img-thumb-bg" style="background-image: url('http://sp-instrumedica.pt/wp-content/uploads/2017/09/UTAD.png'); background-size: 400px; border-radius: 25px ">
                        <div class="overlay"></div>
                        <div class="caption">
                            <div class="tag"><a href="Localizacao.aspx?id=1">UTAD</a></div>
                            <div class="title"><a href="Localizacao.aspx?id=1">Arduinos nesta localização específica</a></div>
                            <div class="content">
                                <p>Dados referentes a todos arduinos implementados nesta localização</p>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
     </div>
    </div>
        <br />
        <div align="center"><h1>Distrito</h1></div>
        <br />
          <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail img-thumb-bg" style="background-image: url('http://utadtv.utad.pt/wp-content/uploads/2016/11/parque_corgo-1500x938.jpg'); background-size: 400px; border-radius: 25px ">
                        <div class="overlay"></div>
                        <div class="caption">
                            <div class="tag"><a href="Distrito.aspx?distrito=Vila Real">Vila Real</a></div>
                            <div class="title"><a href="Distrito.aspx?distrito=Vila Real">Arduinos em Vila Real</a></div>
                            <div class="content">
                                <p>Dados referentes a todos os arduinos de Vila Real</p>
                            </div>
                        </div>
                    </div>
                </div>
    <br />
    <br />
    <p> 
        &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
        <br />
        <br />
            
    </p>
    </div>
</asp:Content>
