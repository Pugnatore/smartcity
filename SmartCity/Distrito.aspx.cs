﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;

namespace SmartCity
{
    public partial class Distrito : System.Web.UI.Page
    {
        private MySqlConnection mConn;
        private MySqlConnection mConn1;
        public string distrito;
        protected void Page_Load(object sender, EventArgs e)
        {
            distrito = Request.QueryString["distrito"];
        }

        protected void Chart4_Load(object sender, EventArgs e)
        {
            
            List<leituras> listaleituras = new List<leituras>();
            List<int> temp = new List<int>();
            List<int> j = new List<int>();
            for (int i = 0; i < 12; i++)
            {
                int q = 0;
                j.Add(q);
                temp.Add(q);
            }
            mConn = new MySqlConnection("Persist Security Info=False;" + "server=127.0.0.1;database=bancoarduino;uid=root");
            mConn.Open();

            if (mConn.State == ConnectionState.Open)
            {
                string query = "SELECT  leituras.id_leituras, leituras.sensor_humidade,leituras.sensor_temperatura,leituras.sensor_luminosidade,leituras.data_leitura,leituras.id_arduino,leituras.id_localizacao FROM `localizacao` INNER JOIN `leituras` ON localizacao.distrito = '"+ distrito +"' and localizacao.id_localizacao = leituras.id_localizacao";
                MySqlCommand command = new MySqlCommand(query, mConn);
                MySqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    leituras let = new leituras();
                    let.Sensor_temperatura = (string)dr["sensor_temperatura"];
                    let.Data = (DateTime)dr["data_leitura"];

                    listaleituras.Add(let);
                }
            }

            Chart4.Series.Clear();
            Chart4.Titles.Add("Temperatura");
            //Chart4.Legends.Add("Temperatura");
            Chart4.Series.Add("Temperatura");
            for (int i = 0; i < listaleituras.Count(); i++)
            {
                switch (listaleituras[i].Data.Month)
                {
                    case 1:

                        if (j[0] == 0)
                        {
                            temp[0] = Convert.ToInt32(listaleituras[i].Sensor_temperatura);
                        }
                        else
                        {
                            temp[0] = (temp[0] + Convert.ToInt32(listaleituras[i].Sensor_temperatura));
                        }
                        j[0]++;
                        break;
                    case 2:
                        if (j[1] == 0)
                        {
                            temp[1] = Convert.ToInt32(listaleituras[i].Sensor_temperatura);
                        }
                        else
                        {
                            temp[1] = (temp[1] + Convert.ToInt32(listaleituras[i].Sensor_temperatura));
                        }
                        j[1]++;
                        break;
                    case 3:
                        if (j[2] == 0)
                        {
                            temp[2] = Convert.ToInt32(listaleituras[i].Sensor_temperatura);
                        }
                        else
                        {
                            temp[2] = (temp[2] + Convert.ToInt32(listaleituras[i].Sensor_temperatura));
                        }
                        j[2]++;
                        break;
                    case 4:
                        if (j[3] == 0)
                        {
                            temp[3] = Convert.ToInt32(listaleituras[i].Sensor_temperatura);
                        }
                        else
                        {
                            temp[3] = (temp[3] + Convert.ToInt32(listaleituras[i].Sensor_temperatura));
                        }
                        j[3]++;
                        break;
                    case 5:
                        if (j[4] == 0)
                        {
                            temp[4] = Convert.ToInt32(listaleituras[i].Sensor_temperatura);
                        }
                        else
                        {
                            temp[4] = (temp[4] + Convert.ToInt32(listaleituras[i].Sensor_temperatura));
                        }
                        j[4]++;
                        break;
                    case 6:
                        if (j[5] == 0)
                        {
                            temp[5] = Convert.ToInt32(listaleituras[i].Sensor_temperatura);
                        }
                        else
                        {
                            temp[5] = (temp[5] + Convert.ToInt32(listaleituras[i].Sensor_temperatura));
                        }
                        j[5]++;
                        break;
                    case 7:
                        if (j[6] == 0)
                        {
                            temp[6] = Convert.ToInt32(listaleituras[i].Sensor_temperatura);
                        }
                        else
                        {
                            temp[6] = (temp[6] + Convert.ToInt32(listaleituras[i].Sensor_temperatura));
                        }
                        j[6]++;
                        break;
                    case 8:
                        if (j[7] == 0)
                        {
                            temp[7] = Convert.ToInt32(listaleituras[i].Sensor_temperatura);
                        }
                        else
                        {
                            temp[7] = (temp[7] + Convert.ToInt32(listaleituras[i].Sensor_temperatura));
                        }
                        j[7]++;
                        break;
                    case 9:
                        if (j[8] == 0)
                        {
                            temp[8] = Convert.ToInt32(listaleituras[i].Sensor_temperatura);
                        }
                        else
                        {
                            temp[8] = (temp[8] + Convert.ToInt32(listaleituras[i].Sensor_temperatura));
                        }
                        j[8]++;
                        break;
                    case 10:
                        if (j[9] == 0)
                        {
                            temp[9] = Convert.ToInt32(listaleituras[i].Sensor_temperatura);
                        }
                        else
                        {
                            temp[9] = (temp[9] + Convert.ToInt32(listaleituras[i].Sensor_temperatura));
                        }
                        j[9]++;
                        break;
                    case 11:
                        if (j[10] == 0)
                        {
                            temp[10] = Convert.ToInt32(listaleituras[i].Sensor_temperatura);
                        }
                        else
                        {
                            temp[10] = (temp[10] + Convert.ToInt32(listaleituras[i].Sensor_temperatura));
                        }
                        j[10]++;
                        break;
                    case 12:
                        if (j[11] == 0)
                        {
                            temp[11] = Convert.ToInt32(listaleituras[i].Sensor_temperatura);
                        }
                        else
                        {
                            temp[11] = (temp[11] + Convert.ToInt32(listaleituras[i].Sensor_temperatura));
                        }
                        j[11]++;
                        break;
                    default:
                        break;
                }

            }
            for (int i = 0; i < 12; i++)
            {
                if (j[i] != 0)
                    temp[i] = temp[i] / j[i];
            }
            Chart4.Series["Temperatura"].Points.AddXY("Janeiro", temp[0]);
            Chart4.Series["Temperatura"].Points.AddXY("Fevereiro", temp[1]);
            Chart4.Series["Temperatura"].Points.AddXY("Março", temp[2]);
            Chart4.Series["Temperatura"].Points.AddXY("Abril", temp[3]);
            Chart4.Series["Temperatura"].Points.AddXY("Maio", temp[4]);
            Chart4.Series["Temperatura"].Points.AddXY("Junho", temp[5]);
            Chart4.Series["Temperatura"].Points.AddXY("Julho", temp[6]);
            Chart4.Series["Temperatura"].Points.AddXY("Agosto", temp[7]);
            Chart4.Series["Temperatura"].Points.AddXY("Setembro", temp[8]);
            Chart4.Series["Temperatura"].Points.AddXY("Outubro", temp[9]);
            Chart4.Series["Temperatura"].Points.AddXY("Novembro", temp[10]);
            Chart4.Series["Temperatura"].Points.AddXY("Dezembro", temp[11]);

        }

        protected void Chart5_Load(object sender, EventArgs e)
        {
            
            List<leituras> listaleituras = new List<leituras>();
            List<int> hum = new List<int>();
            List<int> j = new List<int>();
            for (int i = 0; i < 12; i++)
            {
                int q = 0;
                j.Add(q);
                hum.Add(q);
            }
            mConn = new MySqlConnection("Persist Security Info=False;" + "server=127.0.0.1;database=bancoarduino;uid=root");
            mConn.Open();

            if (mConn.State == ConnectionState.Open)
            {
                string query = "SELECT  leituras.id_leituras, leituras.sensor_humidade,leituras.sensor_temperatura,leituras.sensor_luminosidade,leituras.data_leitura,leituras.id_arduino,leituras.id_localizacao FROM `localizacao` INNER JOIN `leituras` ON localizacao.distrito = '" + distrito + "' and localizacao.id_localizacao = leituras.id_localizacao";
                MySqlCommand command = new MySqlCommand(query, mConn);
                MySqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    leituras let = new leituras();
                    let.Sensor_humidade = (string)dr["sensor_humidade"];
                    let.Data = (DateTime)dr["data_leitura"];

                    listaleituras.Add(let);
                }
            }

            Chart5.Series.Clear();
            Chart5.Titles.Add("Humidade");
            //Chart4.Legends.Add("Humidade");
            Chart5.Series.Add("Humidade");
            for (int i = 0; i < listaleituras.Count(); i++)
            {
                switch (listaleituras[i].Data.Month)
                {
                    case 1:

                        if (j[0] == 0)
                        {
                            hum[0] = Convert.ToInt32(listaleituras[i].Sensor_humidade);
                        }
                        else
                        {
                            hum[0] = (hum[0] + Convert.ToInt32(listaleituras[i].Sensor_humidade));

                        }
                        j[0]++;
                        break;
                    case 2:
                        if (j[1] == 0)
                        {
                            hum[1] = Convert.ToInt32(listaleituras[i].Sensor_humidade);
                        }
                        else
                        {
                            hum[1] = (hum[1] + Convert.ToInt32(listaleituras[i].Sensor_humidade));
                        }
                        j[1]++;
                        break;
                    case 3:
                        if (j[2] == 0)
                        {
                            hum[2] = Convert.ToInt32(listaleituras[i].Sensor_humidade);
                        }
                        else
                        {
                            hum[2] = (hum[2] + Convert.ToInt32(listaleituras[i].Sensor_humidade));
                        }
                        j[2]++;
                        break;
                    case 4:
                        if (j[3] == 0)
                        {
                            hum[3] = Convert.ToInt32(listaleituras[i].Sensor_humidade);
                        }
                        else
                        {
                            hum[3] = (hum[3] + Convert.ToInt32(listaleituras[i].Sensor_humidade));
                        }
                        j[3]++;
                        break;
                    case 5:
                        if (j[4] == 0)
                        {
                            hum[4] = Convert.ToInt32(listaleituras[i].Sensor_humidade);
                        }
                        else
                        {
                            hum[4] = (hum[4] + Convert.ToInt32(listaleituras[i].Sensor_humidade));
                        }
                        j[4]++;
                        break;
                    case 6:
                        if (j[5] == 0)
                        {
                            hum[5] = Convert.ToInt32(listaleituras[i].Sensor_humidade);
                        }
                        else
                        {
                            hum[5] = (hum[5] + Convert.ToInt32(listaleituras[i].Sensor_humidade));
                        }
                        j[5]++;
                        break;
                    case 7:
                        if (j[6] == 0)
                        {
                            hum[6] = Convert.ToInt32(listaleituras[i].Sensor_humidade);
                        }
                        else
                        {
                            hum[6] = (hum[6] + Convert.ToInt32(listaleituras[i].Sensor_humidade));
                        }
                        j[6]++;
                        break;
                    case 8:
                        if (j[7] == 0)
                        {
                            hum[7] = Convert.ToInt32(listaleituras[i].Sensor_humidade);
                        }
                        else
                        {
                            hum[7] = (hum[7] + Convert.ToInt32(listaleituras[i].Sensor_humidade));
                        }
                        j[7]++;
                        break;
                    case 9:
                        if (j[8] == 0)
                        {
                            hum[8] = Convert.ToInt32(listaleituras[i].Sensor_humidade);
                        }
                        else
                        {
                            hum[8] = (hum[8] + Convert.ToInt32(listaleituras[i].Sensor_humidade));
                        }
                        j[8]++;
                        break;
                    case 10:
                        if (j[9] == 0)
                        {
                            hum[9] = Convert.ToInt32(listaleituras[i].Sensor_humidade);
                        }
                        else
                        {
                            hum[9] = (hum[9] + Convert.ToInt32(listaleituras[i].Sensor_humidade));
                        }
                        j[9]++;
                        break;
                    case 11:
                        if (j[10] == 0)
                        {
                            hum[10] = Convert.ToInt32(listaleituras[i].Sensor_humidade);
                        }
                        else
                        {
                            hum[10] = (hum[10] + Convert.ToInt32(listaleituras[i].Sensor_humidade));
                        }
                        j[10]++;
                        break;
                    case 12:
                        if (j[11] == 0)
                        {
                            hum[11] = Convert.ToInt32(listaleituras[i].Sensor_humidade);
                        }
                        else
                        {
                            hum[11] = (hum[11] + Convert.ToInt32(listaleituras[i].Sensor_humidade));
                        }
                        j[11]++;
                        break;
                    default:
                        break;
                }

            }
            for (int i = 0; i < 12; i++)
            {
                if (j[i] != 0)
                    hum[i] = hum[i] / j[i];
            }
            Chart5.Series["Humidade"].Points.AddXY("Janeiro", hum[0]);
            Chart5.Series["Humidade"].Points.AddXY("Fevereiro", hum[1]);
            Chart5.Series["Humidade"].Points.AddXY("Março", hum[2]);
            Chart5.Series["Humidade"].Points.AddXY("Abril", hum[3]);
            Chart5.Series["Humidade"].Points.AddXY("Maio", hum[4]);
            Chart5.Series["Humidade"].Points.AddXY("Junho", hum[5]);
            Chart5.Series["Humidade"].Points.AddXY("Julho", hum[6]);
            Chart5.Series["Humidade"].Points.AddXY("Agosto", hum[7]);
            Chart5.Series["Humidade"].Points.AddXY("Setembro", hum[8]);
            Chart5.Series["Humidade"].Points.AddXY("Outubro", hum[9]);
            Chart5.Series["Humidade"].Points.AddXY("Novembro", hum[10]);
            Chart5.Series["Humidade"].Points.AddXY("Dezembro", hum[11]);

        }

        protected void Chart6_Load(object sender, EventArgs e)
        {
            
            List<leituras> listaleituras = new List<leituras>();
            List<int> lum = new List<int>();
            List<int> j = new List<int>();
            for (int i = 0; i < 12; i++)
            {
                int q = 0;
                j.Add(q);
                lum.Add(q);
            }
            mConn = new MySqlConnection("Persist Security Info=False;" + "server=127.0.0.1;database=bancoarduino;uid=root");
            mConn.Open();

            if (mConn.State == ConnectionState.Open)
            {
                string query = "SELECT  leituras.id_leituras, leituras.sensor_humidade,leituras.sensor_temperatura,leituras.sensor_luminosidade,leituras.data_leitura,leituras.id_arduino,leituras.id_localizacao FROM `localizacao` INNER JOIN `leituras` ON localizacao.distrito = '" + distrito + "' and localizacao.id_localizacao = leituras.id_localizacao";
                MySqlCommand command = new MySqlCommand(query, mConn);
                MySqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    leituras let = new leituras();
                    let.Sensor_luminosidade = (string)dr["sensor_luminosidade"];
                    let.Data = (DateTime)dr["data_leitura"];

                    listaleituras.Add(let);
                }
            }

            Chart6.Series.Clear();
            Chart6.Titles.Add("Luminosidade");
            //Chart4.Legends.Add("Luminosidade");
            Chart6.Series.Add("Luminosidade");

            for (int i = 0; i < listaleituras.Count(); i++)
            {
                switch (listaleituras[i].Data.Month)
                {
                    case 1:

                        if (j[0] == 0)
                        {
                            lum[0] = Convert.ToInt32(listaleituras[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lum[0] = (lum[0] + Convert.ToInt32(listaleituras[i].Sensor_luminosidade));

                        }
                        j[0]++;
                        break;
                    case 2:
                        if (j[1] == 0)
                        {
                            lum[1] = Convert.ToInt32(listaleituras[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lum[1] = (lum[1] + Convert.ToInt32(listaleituras[i].Sensor_luminosidade));
                        }
                        j[1]++;
                        break;
                    case 3:
                        if (j[2] == 0)
                        {
                            lum[2] = Convert.ToInt32(listaleituras[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lum[2] = (lum[2] + Convert.ToInt32(listaleituras[i].Sensor_luminosidade));
                        }
                        j[2]++;
                        break;
                    case 4:
                        if (j[3] == 0)
                        {
                            lum[3] = Convert.ToInt32(listaleituras[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lum[3] = (lum[3] + Convert.ToInt32(listaleituras[i].Sensor_luminosidade));
                        }
                        j[3]++;
                        break;
                    case 5:
                        if (j[4] == 0)
                        {
                            lum[4] = Convert.ToInt32(listaleituras[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lum[4] = (lum[4] + Convert.ToInt32(listaleituras[i].Sensor_luminosidade));
                        }
                        j[4]++;
                        break;
                    case 6:
                        if (j[5] == 0)
                        {
                            lum[5] = Convert.ToInt32(listaleituras[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lum[5] = (lum[5] + Convert.ToInt32(listaleituras[i].Sensor_luminosidade));
                        }
                        j[5]++;
                        break;
                    case 7:
                        if (j[6] == 0)
                        {
                            lum[6] = Convert.ToInt32(listaleituras[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lum[6] = (lum[6] + Convert.ToInt32(listaleituras[i].Sensor_luminosidade));
                        }
                        j[6]++;
                        break;
                    case 8:
                        if (j[7] == 0)
                        {
                            lum[7] = Convert.ToInt32(listaleituras[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lum[7] = (lum[7] + Convert.ToInt32(listaleituras[i].Sensor_luminosidade));
                        }
                        j[7]++;
                        break;
                    case 9:
                        if (j[8] == 0)
                        {
                            lum[8] = Convert.ToInt32(listaleituras[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lum[8] = (lum[8] + Convert.ToInt32(listaleituras[i].Sensor_luminosidade));
                        }
                        j[8]++;
                        break;
                    case 10:
                        if (j[9] == 0)
                        {
                            lum[9] = Convert.ToInt32(listaleituras[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lum[9] = (lum[9] + Convert.ToInt32(listaleituras[i].Sensor_luminosidade));
                        }
                        j[9]++;
                        break;
                    case 11:
                        if (j[10] == 0)
                        {
                            lum[10] = Convert.ToInt32(listaleituras[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lum[10] = (lum[10] + Convert.ToInt32(listaleituras[i].Sensor_luminosidade));
                        }
                        j[10]++;
                        break;
                    case 12:
                        if (j[11] == 0)
                        {
                            lum[11] = Convert.ToInt32(listaleituras[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lum[11] = (lum[11] + Convert.ToInt32(listaleituras[i].Sensor_luminosidade));
                        }
                        j[11]++;
                        break;
                    default:
                        break;
                }

            }
            for (int i = 0; i < 12; i++)
            {
                if (j[i] != 0)
                    lum[i] = lum[i] / j[i];
            }
            Chart6.Series["Luminosidade"].Points.AddXY("Janeiro", lum[0]);
            Chart6.Series["Luminosidade"].Points.AddXY("Fevereiro", lum[1]);
            Chart6.Series["Luminosidade"].Points.AddXY("Março", lum[2]);
            Chart6.Series["Luminosidade"].Points.AddXY("Abril", lum[3]);
            Chart6.Series["Luminosidade"].Points.AddXY("Maio", lum[4]);
            Chart6.Series["Luminosidade"].Points.AddXY("Junho", lum[5]);
            Chart6.Series["Luminosidade"].Points.AddXY("Julho", lum[6]);
            Chart6.Series["Luminosidade"].Points.AddXY("Agosto", lum[7]);
            Chart6.Series["Luminosidade"].Points.AddXY("Setembro", lum[8]);
            Chart6.Series["Luminosidade"].Points.AddXY("Outubro", lum[9]);
            Chart6.Series["Luminosidade"].Points.AddXY("Novembro", lum[10]);
            Chart6.Series["Luminosidade"].Points.AddXY("Dezembro", lum[11]);
        }

        protected void Chart7_Load(object sender, EventArgs e)
        {
            
            List<leituras> listaleiturasLY = new List<leituras>();
            List<leituras> listaleiturasTY = new List<leituras>();
            List<int> tempLY = new List<int>();
            List<int> tempTY = new List<int>();
            List<int> jLY = new List<int>();
            List<int> jTY = new List<int>();
            for (int i = 0; i < 12; i++)
            {
                int q = 0;
                jLY.Add(q);
                jTY.Add(q);
                tempLY.Add(q);
                tempTY.Add(q);
            }
            mConn = new MySqlConnection("Persist Security Info=False;" + "server=127.0.0.1;database=bancoarduino;uid=root");
            mConn.Open();

            if (mConn.State == ConnectionState.Open)
            {
                string query = "SELECT  leituras.id_leituras, leituras.sensor_humidade,leituras.sensor_temperatura,leituras.sensor_luminosidade,leituras.data_leitura,leituras.id_arduino,leituras.id_localizacao FROM `localizacao` INNER JOIN `leituras` ON localizacao.distrito = '" + distrito + "' and localizacao.id_localizacao = leituras.id_localizacao and YEAR(leituras.data_leitura)= YEAR(CURRENT_DATE) ";
                MySqlCommand command = new MySqlCommand(query, mConn);
                MySqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    leituras let = new leituras();
                    let.Sensor_temperatura = (string)dr["sensor_temperatura"];
                    let.Data = (DateTime)dr["data_leitura"];

                    listaleiturasTY.Add(let);
                }
                mConn1 = new MySqlConnection("Persist Security Info=False;" + "server=127.0.0.1;database=bancoarduino;uid=root");
                mConn1.Open();

                if (mConn1.State == ConnectionState.Open)
                {
                    string query1="SELECT  leituras.id_leituras, leituras.sensor_humidade,leituras.sensor_temperatura,leituras.sensor_luminosidade,leituras.data_leitura,leituras.id_arduino,leituras.id_localizacao FROM `localizacao` INNER JOIN `leituras` ON localizacao.distrito = '" + distrito + "' and localizacao.id_localizacao = leituras.id_localizacao and YEAR(leituras.data_leitura)= YEAR(CURRENT_DATE) - 1 ";
                    MySqlCommand command1 = new MySqlCommand(query1, mConn1);
                    MySqlDataReader dr1 = command1.ExecuteReader();

                    while (dr1.Read())
                    {
                        leituras let = new leituras();
                        let.Sensor_temperatura = (string)dr1["sensor_temperatura"];
                        let.Data = (DateTime)dr1["data_leitura"];

                        listaleiturasLY.Add(let);
                    }
                }
            }

            for (int i = 0; i < listaleiturasLY.Count(); i++)
            {
                switch (listaleiturasLY[i].Data.Month)
                {
                    case 1:

                        if (jLY[0] == 0)
                        {
                            tempLY[0] = Convert.ToInt32(listaleiturasLY[i].Sensor_temperatura);
                        }
                        else
                        {
                            tempLY[0] = (tempLY[0] + Convert.ToInt32(listaleiturasLY[i].Sensor_temperatura));
                        }
                        jLY[0]++;
                        break;
                    case 2:
                        if (jLY[1] == 0)
                        {
                            tempLY[1] = Convert.ToInt32(listaleiturasLY[i].Sensor_temperatura);
                        }
                        else
                        {
                            tempLY[1] = (tempLY[1] + Convert.ToInt32(listaleiturasLY[i].Sensor_temperatura));
                        }
                        jLY[1]++;
                        break;
                    case 3:
                        if (jLY[2] == 0)
                        {
                            tempLY[2] = Convert.ToInt32(listaleiturasLY[i].Sensor_temperatura);
                        }
                        else
                        {
                            tempLY[2] = (tempLY[2] + Convert.ToInt32(listaleiturasLY[i].Sensor_temperatura));
                        }
                        jLY[2]++;
                        break;
                    case 4:
                        if (jLY[3] == 0)
                        {
                            tempLY[3] = Convert.ToInt32(listaleiturasLY[i].Sensor_temperatura);
                        }
                        else
                        {
                            tempLY[3] = (tempLY[3] + Convert.ToInt32(listaleiturasLY[i].Sensor_temperatura));
                        }
                        jLY[3]++;
                        break;
                    case 5:
                        if (jLY[4] == 0)
                        {
                            tempLY[4] = Convert.ToInt32(listaleiturasLY[i].Sensor_temperatura);
                        }
                        else
                        {
                            tempLY[4] = (tempLY[4] + Convert.ToInt32(listaleiturasLY[i].Sensor_temperatura));
                        }
                        jLY[4]++;
                        break;
                    case 6:
                        if (jLY[5] == 0)
                        {
                            tempLY[5] = Convert.ToInt32(listaleiturasLY[i].Sensor_temperatura);
                        }
                        else
                        {
                            tempLY[5] = (tempLY[5] + Convert.ToInt32(listaleiturasLY[i].Sensor_temperatura));
                        }
                        jLY[5]++;
                        break;
                    case 7:
                        if (jLY[6] == 0)
                        {
                            tempLY[6] = Convert.ToInt32(listaleiturasLY[i].Sensor_temperatura);
                        }
                        else
                        {
                            tempLY[6] = (tempLY[6] + Convert.ToInt32(listaleiturasLY[i].Sensor_temperatura));
                        }
                        jLY[6]++;
                        break;
                    case 8:
                        if (jLY[7] == 0)
                        {
                            tempLY[7] = Convert.ToInt32(listaleiturasLY[i].Sensor_temperatura);
                        }
                        else
                        {
                            tempLY[7] = (tempLY[7] + Convert.ToInt32(listaleiturasLY[i].Sensor_temperatura));
                        }
                        jLY[7]++;
                        break;
                    case 9:
                        if (jLY[8] == 0)
                        {
                            tempLY[8] = Convert.ToInt32(listaleiturasLY[i].Sensor_temperatura);
                        }
                        else
                        {
                            tempLY[8] = (tempLY[8] + Convert.ToInt32(listaleiturasLY[i].Sensor_temperatura));
                        }
                        jLY[8]++;
                        break;
                    case 10:
                        if (jLY[9] == 0)
                        {
                            tempLY[9] = Convert.ToInt32(listaleiturasLY[i].Sensor_temperatura);
                        }
                        else
                        {
                            tempLY[9] = (tempLY[9] + Convert.ToInt32(listaleiturasLY[i].Sensor_temperatura));
                        }
                        jLY[9]++;
                        break;
                    case 11:
                        if (jLY[10] == 0)
                        {
                            tempLY[10] = Convert.ToInt32(listaleiturasLY[i].Sensor_temperatura);
                        }
                        else
                        {
                            tempLY[10] = (tempLY[10] + Convert.ToInt32(listaleiturasLY[i].Sensor_temperatura));
                        }
                        jLY[10]++;
                        break;
                    case 12:
                        if (jLY[11] == 0)
                        {
                            tempLY[11] = Convert.ToInt32(listaleiturasLY[i].Sensor_temperatura);
                        }
                        else
                        {
                            tempLY[11] = (tempLY[11] + Convert.ToInt32(listaleiturasLY[i].Sensor_temperatura));
                        }
                        jLY[11]++;
                        break;
                    default:
                        break;
                }

            }
            for (int i = 0; i < 12; i++)
            {
                if (jLY[i] != 0)
                    tempLY[i] = tempLY[i] / jLY[i];
            }

            for (int i = 0; i < listaleiturasTY.Count(); i++)
            {
                switch (listaleiturasTY[i].Data.Month)
                {
                    case 1:

                        if (jTY[0] == 0)
                        {
                            tempTY[0] = Convert.ToInt32(listaleiturasTY[i].Sensor_temperatura);
                        }
                        else
                        {
                            tempTY[0] = (tempTY[0] + Convert.ToInt32(listaleiturasTY[i].Sensor_temperatura));
                        }
                        jTY[0]++;
                        break;
                    case 2:
                        if (jTY[1] == 0)
                        {
                            tempTY[1] = Convert.ToInt32(listaleiturasTY[i].Sensor_temperatura);
                        }
                        else
                        {
                            tempTY[1] = (tempTY[1] + Convert.ToInt32(listaleiturasTY[i].Sensor_temperatura));
                        }
                        jTY[1]++;
                        break;
                    case 3:
                        if (jTY[2] == 0)
                        {
                            tempTY[2] = Convert.ToInt32(listaleiturasTY[i].Sensor_temperatura);
                        }
                        else
                        {
                            tempTY[2] = (tempTY[2] + Convert.ToInt32(listaleiturasTY[i].Sensor_temperatura));
                        }
                        jTY[2]++;
                        break;
                    case 4:
                        if (jTY[3] == 0)
                        {
                            tempTY[3] = Convert.ToInt32(listaleiturasTY[i].Sensor_temperatura);
                        }
                        else
                        {
                            tempTY[3] = (tempTY[3] + Convert.ToInt32(listaleiturasTY[i].Sensor_temperatura));
                        }
                        jTY[3]++;
                        break;
                    case 5:
                        if (jTY[4] == 0)
                        {
                            tempTY[4] = Convert.ToInt32(listaleiturasTY[i].Sensor_temperatura);
                        }
                        else
                        {
                            tempTY[4] = (tempTY[4] + Convert.ToInt32(listaleiturasTY[i].Sensor_temperatura));
                        }
                        jTY[4]++;
                        break;
                    case 6:
                        if (jTY[5] == 0)
                        {
                            tempTY[5] = Convert.ToInt32(listaleiturasTY[i].Sensor_temperatura);
                        }
                        else
                        {
                            tempTY[5] = (tempTY[5] + Convert.ToInt32(listaleiturasTY[i].Sensor_temperatura));
                        }
                        jTY[5]++;
                        break;
                    case 7:
                        if (jTY[6] == 0)
                        {
                            tempTY[6] = Convert.ToInt32(listaleiturasTY[i].Sensor_temperatura);
                        }
                        else
                        {
                            tempTY[6] = (tempTY[6] + Convert.ToInt32(listaleiturasTY[i].Sensor_temperatura));
                        }
                        jTY[6]++;
                        break;
                    case 8:
                        if (jTY[7] == 0)
                        {
                            tempTY[7] = Convert.ToInt32(listaleiturasTY[i].Sensor_temperatura);
                        }
                        else
                        {
                            tempTY[7] = (tempTY[7] + Convert.ToInt32(listaleiturasTY[i].Sensor_temperatura));
                        }
                        jTY[7]++;
                        break;
                    case 9:
                        if (jTY[8] == 0)
                        {
                            tempTY[8] = Convert.ToInt32(listaleiturasTY[i].Sensor_temperatura);
                        }
                        else
                        {
                            tempTY[8] = (tempTY[8] + Convert.ToInt32(listaleiturasTY[i].Sensor_temperatura));
                        }
                        jTY[8]++;
                        break;
                    case 10:
                        if (jTY[9] == 0)
                        {
                            tempTY[9] = Convert.ToInt32(listaleiturasTY[i].Sensor_temperatura);
                        }
                        else
                        {
                            tempTY[9] = (tempTY[9] + Convert.ToInt32(listaleiturasTY[i].Sensor_temperatura));
                        }
                        jTY[9]++;
                        break;
                    case 11:
                        if (jTY[10] == 0)
                        {
                            tempTY[10] = Convert.ToInt32(listaleiturasTY[i].Sensor_temperatura);
                        }
                        else
                        {
                            tempTY[10] = (tempTY[10] + Convert.ToInt32(listaleiturasTY[i].Sensor_temperatura));
                        }
                        jTY[10]++;
                        break;
                    case 12:
                        if (jTY[11] == 0)
                        {
                            tempTY[11] = Convert.ToInt32(listaleiturasTY[i].Sensor_temperatura);
                        }
                        else
                        {
                            tempTY[11] = (tempTY[11] + Convert.ToInt32(listaleiturasTY[i].Sensor_temperatura));
                        }
                        jTY[11]++;
                        break;
                    default:
                        break;
                }

            }
            for (int i = 0; i < 12; i++)
            {
                if (jTY[i] != 0)
                    tempTY[i] = tempTY[i] / jTY[i];
            }

            Chart7.Series.Clear();
            Chart7.Titles.Add("Temperatura");
            Chart7.Legends.Add("Temperatura");


            Series series = Chart7.Series.Add("Ano 2018");
            series.ChartType = SeriesChartType.Spline;
            series.Points.AddXY("Janeiro", tempTY[0]);
            series.Points.AddXY("Fevereiro", tempTY[1]);
            series.Points.AddXY("Maarço", tempTY[2]);
            series.Points.AddXY("Abril", tempTY[3]);
            series.Points.AddXY("Maio", tempTY[4]);
            series.Points.AddXY("Junho", tempTY[5]);
            series.Points.AddXY("Julho", tempTY[6]);
            series.Points.AddXY("Agosto", tempTY[7]);
            series.Points.AddXY("Setembro", tempTY[8]);
            series.Points.AddXY("Outubro", tempTY[9]);
            series.Points.AddXY("Novembro", tempTY[10]);
            series.Points.AddXY("Dezembro", tempTY[11]);
            Series series1 = Chart7.Series.Add("Ano 2017");
            series1.ChartType = SeriesChartType.Spline;
            series1.Points.AddXY("Janeiro", tempLY[0]);
            series1.Points.AddXY("Fevereiro", tempLY[1]);
            series1.Points.AddXY("Maarço", tempLY[2]);
            series1.Points.AddXY("Abril", tempLY[3]);
            series1.Points.AddXY("Maio", tempLY[4]);
            series1.Points.AddXY("Junho", tempLY[5]);
            series1.Points.AddXY("Julho", tempLY[6]);
            series1.Points.AddXY("Agosto", tempLY[7]);
            series1.Points.AddXY("Setembro", tempLY[8]);
            series1.Points.AddXY("Outubro", tempLY[9]);
            series1.Points.AddXY("Novembro", tempLY[10]);
            series1.Points.AddXY("Dezembro", tempLY[11]);
        }

        protected void Chart8_Load(object sender, EventArgs e)
        {
            
            List<leituras> listaleiturasLY = new List<leituras>();
            List<leituras> listaleiturasTY = new List<leituras>();
            List<int> humLY = new List<int>();
            List<int> humTY = new List<int>();
            List<int> jLY = new List<int>();
            List<int> jTY = new List<int>();
            for (int i = 0; i < 12; i++)
            {
                int q = 0;
                jLY.Add(q);
                jTY.Add(q);
                humLY.Add(q);
                humTY.Add(q);
            }
            mConn = new MySqlConnection("Persist Security Info=False;" + "server=127.0.0.1;database=bancoarduino;uid=root");
            mConn.Open();

            if (mConn.State == ConnectionState.Open)
            {
                string query = "SELECT  leituras.id_leituras, leituras.sensor_humidade,leituras.sensor_temperatura,leituras.sensor_luminosidade,leituras.data_leitura,leituras.id_arduino,leituras.id_localizacao FROM `localizacao` INNER JOIN `leituras` ON localizacao.distrito = '" + distrito + "' and localizacao.id_localizacao = leituras.id_localizacao and YEAR(leituras.data_leitura)= YEAR(CURRENT_DATE) ";
                MySqlCommand command = new MySqlCommand(query, mConn);
                MySqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    leituras let = new leituras();
                    let.Sensor_humidade = (string)dr["sensor_humidade"];
                    let.Data = (DateTime)dr["data_leitura"];

                    listaleiturasTY.Add(let);
                }
                mConn1 = new MySqlConnection("Persist Security Info=False;" + "server=127.0.0.1;database=bancoarduino;uid=root");
                mConn1.Open();

                if (mConn1.State == ConnectionState.Open)
                {
                    string query1 = "SELECT  leituras.id_leituras, leituras.sensor_humidade,leituras.sensor_temperatura,leituras.sensor_luminosidade,leituras.data_leitura,leituras.id_arduino,leituras.id_localizacao FROM `localizacao` INNER JOIN `leituras` ON localizacao.distrito = '" + distrito + "' and localizacao.id_localizacao = leituras.id_localizacao and YEAR(leituras.data_leitura)= YEAR(CURRENT_DATE) - 1 ";
                    MySqlCommand command1 = new MySqlCommand(query1, mConn1);
                    MySqlDataReader dr1 = command1.ExecuteReader();

                    while (dr1.Read())
                    {
                        leituras let = new leituras();
                        let.Sensor_humidade = (string)dr1["sensor_humidade"];
                        let.Data = (DateTime)dr1["data_leitura"];

                        listaleiturasLY.Add(let);
                    }
                }
            }

            for (int i = 0; i < listaleiturasLY.Count(); i++)
            {
                switch (listaleiturasLY[i].Data.Month)
                {
                    case 1:

                        if (jLY[0] == 0)
                        {
                            humLY[0] = Convert.ToInt32(listaleiturasLY[i].Sensor_humidade);
                        }
                        else
                        {
                            humLY[0] = (humLY[0] + Convert.ToInt32(listaleiturasLY[i].Sensor_humidade));
                        }
                        jLY[0]++;
                        break;
                    case 2:
                        if (jLY[1] == 0)
                        {
                            humLY[1] = Convert.ToInt32(listaleiturasLY[i].Sensor_humidade);
                        }
                        else
                        {
                            humLY[1] = (humLY[1] + Convert.ToInt32(listaleiturasLY[i].Sensor_humidade));
                        }
                        jLY[1]++;
                        break;
                    case 3:
                        if (jLY[2] == 0)
                        {
                            humLY[2] = Convert.ToInt32(listaleiturasLY[i].Sensor_humidade);
                        }
                        else
                        {
                            humLY[2] = (humLY[2] + Convert.ToInt32(listaleiturasLY[i].Sensor_humidade));
                        }
                        jLY[2]++;
                        break;
                    case 4:
                        if (jLY[3] == 0)
                        {
                            humLY[3] = Convert.ToInt32(listaleiturasLY[i].Sensor_humidade);
                        }
                        else
                        {
                            humLY[3] = (humLY[3] + Convert.ToInt32(listaleiturasLY[i].Sensor_humidade));
                        }
                        jLY[3]++;
                        break;
                    case 5:
                        if (jLY[4] == 0)
                        {
                            humLY[4] = Convert.ToInt32(listaleiturasLY[i].Sensor_humidade);
                        }
                        else
                        {
                            humLY[4] = (humLY[4] + Convert.ToInt32(listaleiturasLY[i].Sensor_humidade));
                        }
                        jLY[4]++;
                        break;
                    case 6:
                        if (jLY[5] == 0)
                        {
                            humLY[5] = Convert.ToInt32(listaleiturasLY[i].Sensor_humidade);
                        }
                        else
                        {
                            humLY[5] = (humLY[5] + Convert.ToInt32(listaleiturasLY[i].Sensor_humidade));
                        }
                        jLY[5]++;
                        break;
                    case 7:
                        if (jLY[6] == 0)
                        {
                            humLY[6] = Convert.ToInt32(listaleiturasLY[i].Sensor_humidade);
                        }
                        else
                        {
                            humLY[6] = (humLY[6] + Convert.ToInt32(listaleiturasLY[i].Sensor_humidade));
                        }
                        jLY[6]++;
                        break;
                    case 8:
                        if (jLY[7] == 0)
                        {
                            humLY[7] = Convert.ToInt32(listaleiturasLY[i].Sensor_humidade);
                        }
                        else
                        {
                            humLY[7] = (humLY[7] + Convert.ToInt32(listaleiturasLY[i].Sensor_humidade));
                        }
                        jLY[7]++;
                        break;
                    case 9:
                        if (jLY[8] == 0)
                        {
                            humLY[8] = Convert.ToInt32(listaleiturasLY[i].Sensor_humidade);
                        }
                        else
                        {
                            humLY[8] = (humLY[8] + Convert.ToInt32(listaleiturasLY[i].Sensor_humidade));
                        }
                        jLY[8]++;
                        break;
                    case 10:
                        if (jLY[9] == 0)
                        {
                            humLY[9] = Convert.ToInt32(listaleiturasLY[i].Sensor_humidade);
                        }
                        else
                        {
                            humLY[9] = (humLY[9] + Convert.ToInt32(listaleiturasLY[i].Sensor_humidade));
                        }
                        jLY[9]++;
                        break;
                    case 11:
                        if (jLY[10] == 0)
                        {
                            humLY[10] = Convert.ToInt32(listaleiturasLY[i].Sensor_humidade);
                        }
                        else
                        {
                            humLY[10] = (humLY[10] + Convert.ToInt32(listaleiturasLY[i].Sensor_humidade));
                        }
                        jLY[10]++;
                        break;
                    case 12:
                        if (jLY[11] == 0)
                        {
                            humLY[11] = Convert.ToInt32(listaleiturasLY[i].Sensor_humidade);
                        }
                        else
                        {
                            humLY[11] = (humLY[11] + Convert.ToInt32(listaleiturasLY[i].Sensor_humidade));
                        }
                        jLY[11]++;
                        break;
                    default:
                        break;
                }

            }
            for (int i = 0; i < 12; i++)
            {
                if (jLY[i] != 0)
                    humLY[i] = humLY[i] / jLY[i];
            }

            for (int i = 0; i < listaleiturasTY.Count(); i++)
            {
                switch (listaleiturasTY[i].Data.Month)
                {
                    case 1:

                        if (jTY[0] == 0)
                        {
                            humTY[0] = Convert.ToInt32(listaleiturasTY[i].Sensor_humidade);
                        }
                        else
                        {
                            humTY[0] = (humTY[0] + Convert.ToInt32(listaleiturasTY[i].Sensor_humidade));
                        }
                        jTY[0]++;
                        break;
                    case 2:
                        if (jTY[1] == 0)
                        {
                            humTY[1] = Convert.ToInt32(listaleiturasTY[i].Sensor_humidade);
                        }
                        else
                        {
                            humTY[1] = (humTY[1] + Convert.ToInt32(listaleiturasTY[i].Sensor_humidade));
                        }
                        jTY[1]++;
                        break;
                    case 3:
                        if (jTY[2] == 0)
                        {
                            humTY[2] = Convert.ToInt32(listaleiturasTY[i].Sensor_humidade);
                        }
                        else
                        {
                            humTY[2] = (humTY[2] + Convert.ToInt32(listaleiturasTY[i].Sensor_humidade));
                        }
                        jTY[2]++;
                        break;
                    case 4:
                        if (jTY[3] == 0)
                        {
                            humTY[3] = Convert.ToInt32(listaleiturasTY[i].Sensor_humidade);
                        }
                        else
                        {
                            humTY[3] = (humTY[3] + Convert.ToInt32(listaleiturasTY[i].Sensor_humidade));
                        }
                        jTY[3]++;
                        break;
                    case 5:
                        if (jTY[4] == 0)
                        {
                            humTY[4] = Convert.ToInt32(listaleiturasTY[i].Sensor_humidade);
                        }
                        else
                        {
                            humTY[4] = (humTY[4] + Convert.ToInt32(listaleiturasTY[i].Sensor_humidade));
                        }
                        jTY[4]++;
                        break;
                    case 6:
                        if (jTY[5] == 0)
                        {
                            humTY[5] = Convert.ToInt32(listaleiturasTY[i].Sensor_humidade);
                        }
                        else
                        {
                            humTY[5] = (humTY[5] + Convert.ToInt32(listaleiturasTY[i].Sensor_humidade));
                        }
                        jTY[5]++;
                        break;
                    case 7:
                        if (jTY[6] == 0)
                        {
                            humTY[6] = Convert.ToInt32(listaleiturasTY[i].Sensor_humidade);
                        }
                        else
                        {
                            humTY[6] = (humTY[6] + Convert.ToInt32(listaleiturasTY[i].Sensor_humidade));
                        }
                        jTY[6]++;
                        break;
                    case 8:
                        if (jTY[7] == 0)
                        {
                            humTY[7] = Convert.ToInt32(listaleiturasTY[i].Sensor_humidade);
                        }
                        else
                        {
                            humTY[7] = (humTY[7] + Convert.ToInt32(listaleiturasTY[i].Sensor_humidade));
                        }
                        jTY[7]++;
                        break;
                    case 9:
                        if (jTY[8] == 0)
                        {
                            humTY[8] = Convert.ToInt32(listaleiturasTY[i].Sensor_humidade);
                        }
                        else
                        {
                            humTY[8] = (humTY[8] + Convert.ToInt32(listaleiturasTY[i].Sensor_humidade));
                        }
                        jTY[8]++;
                        break;
                    case 10:
                        if (jTY[9] == 0)
                        {
                            humTY[9] = Convert.ToInt32(listaleiturasTY[i].Sensor_humidade);
                        }
                        else
                        {
                            humTY[9] = (humTY[9] + Convert.ToInt32(listaleiturasTY[i].Sensor_humidade));
                        }
                        jTY[9]++;
                        break;
                    case 11:
                        if (jTY[10] == 0)
                        {
                            humTY[10] = Convert.ToInt32(listaleiturasTY[i].Sensor_humidade);
                        }
                        else
                        {
                            humTY[10] = (humTY[10] + Convert.ToInt32(listaleiturasTY[i].Sensor_humidade));
                        }
                        jTY[10]++;
                        break;
                    case 12:
                        if (jTY[11] == 0)
                        {
                            humTY[11] = Convert.ToInt32(listaleiturasTY[i].Sensor_humidade);
                        }
                        else
                        {
                            humTY[11] = (humTY[11] + Convert.ToInt32(listaleiturasTY[i].Sensor_humidade));
                        }
                        jTY[11]++;
                        break;
                    default:
                        break;
                }

            }
            for (int i = 0; i < 12; i++)
            {
                if (jTY[i] != 0)
                    humTY[i] = humTY[i] / jTY[i];
            }

            Chart8.Series.Clear();
            Chart8.Titles.Add("Humidade");
            Chart8.Legends.Add("Humidade");


            Series series = Chart8.Series.Add("Ano 2018");
            series.ChartType = SeriesChartType.Spline;
            series.Points.AddXY("Janeiro", humTY[0]);
            series.Points.AddXY("Fevereiro", humTY[1]);
            series.Points.AddXY("Maarço", humTY[2]);
            series.Points.AddXY("Abril", humTY[3]);
            series.Points.AddXY("Maio", humTY[4]);
            series.Points.AddXY("Junho", humTY[5]);
            series.Points.AddXY("Julho", humTY[6]);
            series.Points.AddXY("Agosto", humTY[7]);
            series.Points.AddXY("Setembro", humTY[8]);
            series.Points.AddXY("Outubro", humTY[9]);
            series.Points.AddXY("Novembro", humTY[10]);
            series.Points.AddXY("Dezembro", humTY[11]);
            Series series1 = Chart8.Series.Add("Ano 2017");
            series1.ChartType = SeriesChartType.Spline;
            series1.Points.AddXY("Janeiro", humLY[0]);
            series1.Points.AddXY("Fevereiro", humLY[1]);
            series1.Points.AddXY("Maarço", humLY[2]);
            series1.Points.AddXY("Abril", humLY[3]);
            series1.Points.AddXY("Maio", humLY[4]);
            series1.Points.AddXY("Junho", humLY[5]);
            series1.Points.AddXY("Julho", humLY[6]);
            series1.Points.AddXY("Agosto", humLY[7]);
            series1.Points.AddXY("Setembro", humLY[8]);
            series1.Points.AddXY("Outubro", humLY[9]);
            series1.Points.AddXY("Novembro", humLY[10]);
            series1.Points.AddXY("Dezembro", humLY[11]);
        }

        protected void Chart9_Load(object sender, EventArgs e)
        {
           
            List<leituras> listaleiturasLY = new List<leituras>();
            List<leituras> listaleiturasTY = new List<leituras>();
            List<int> lumLY = new List<int>();
            List<int> lumTY = new List<int>();
            List<int> jLY = new List<int>();
            List<int> jTY = new List<int>();
            for (int i = 0; i < 12; i++)
            {
                int q = 0;
                jLY.Add(q);
                jTY.Add(q);
                lumLY.Add(q);
                lumTY.Add(q);
            }
            mConn = new MySqlConnection("Persist Security Info=False;" + "server=127.0.0.1;database=bancoarduino;uid=root");
            mConn.Open();

            if (mConn.State == ConnectionState.Open)
            {
                string query = "SELECT  leituras.id_leituras, leituras.sensor_humidade,leituras.sensor_temperatura,leituras.sensor_luminosidade,leituras.data_leitura,leituras.id_arduino,leituras.id_localizacao FROM `localizacao` INNER JOIN `leituras` ON localizacao.distrito = '" + distrito + "' and localizacao.id_localizacao = leituras.id_localizacao and YEAR(leituras.data_leitura)= YEAR(CURRENT_DATE) ";
                MySqlCommand command = new MySqlCommand(query, mConn);
                MySqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    leituras let = new leituras();
                    let.Sensor_luminosidade = (string)dr["sensor_luminosidade"];
                    let.Data = (DateTime)dr["data_leitura"];

                    listaleiturasTY.Add(let);
                }
                mConn1 = new MySqlConnection("Persist Security Info=False;" + "server=127.0.0.1;database=bancoarduino;uid=root");
                mConn1.Open();

                if (mConn1.State == ConnectionState.Open)
                {
                    string query1 = "SELECT  leituras.id_leituras, leituras.sensor_humidade,leituras.sensor_temperatura,leituras.sensor_luminosidade,leituras.data_leitura,leituras.id_arduino,leituras.id_localizacao FROM `localizacao` INNER JOIN `leituras` ON localizacao.distrito = '" + distrito + "' and localizacao.id_localizacao = leituras.id_localizacao and YEAR(leituras.data_leitura)= YEAR(CURRENT_DATE) - 1 ";
                    MySqlCommand command1 = new MySqlCommand(query1, mConn1);
                    MySqlDataReader dr1 = command1.ExecuteReader();

                    while (dr1.Read())
                    {
                        leituras let = new leituras();
                        let.Sensor_luminosidade = (string)dr1["sensor_luminosidade"];
                        let.Data = (DateTime)dr1["data_leitura"];

                        listaleiturasLY.Add(let);
                    }
                }
            }

            for (int i = 0; i < listaleiturasLY.Count(); i++)
            {
                switch (listaleiturasLY[i].Data.Month)
                {
                    case 1:

                        if (jLY[0] == 0)
                        {
                            lumLY[0] = Convert.ToInt32(listaleiturasLY[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lumLY[0] = (lumLY[0] + Convert.ToInt32(listaleiturasLY[i].Sensor_luminosidade));
                        }
                        jLY[0]++;
                        break;
                    case 2:
                        if (jLY[1] == 0)
                        {
                            lumLY[1] = Convert.ToInt32(listaleiturasLY[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lumLY[1] = (lumLY[1] + Convert.ToInt32(listaleiturasLY[i].Sensor_luminosidade));
                        }
                        jLY[1]++;
                        break;
                    case 3:
                        if (jLY[2] == 0)
                        {
                            lumLY[2] = Convert.ToInt32(listaleiturasLY[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lumLY[2] = (lumLY[2] + Convert.ToInt32(listaleiturasLY[i].Sensor_luminosidade));
                        }
                        jLY[2]++;
                        break;
                    case 4:
                        if (jLY[3] == 0)
                        {
                            lumLY[3] = Convert.ToInt32(listaleiturasLY[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lumLY[3] = (lumLY[3] + Convert.ToInt32(listaleiturasLY[i].Sensor_luminosidade));
                        }
                        jLY[3]++;
                        break;
                    case 5:
                        if (jLY[4] == 0)
                        {
                            lumLY[4] = Convert.ToInt32(listaleiturasLY[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lumLY[4] = (lumLY[4] + Convert.ToInt32(listaleiturasLY[i].Sensor_luminosidade));
                        }
                        jLY[4]++;
                        break;
                    case 6:
                        if (jLY[5] == 0)
                        {
                            lumLY[5] = Convert.ToInt32(listaleiturasLY[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lumLY[5] = (lumLY[5] + Convert.ToInt32(listaleiturasLY[i].Sensor_luminosidade));
                        }
                        jLY[5]++;
                        break;
                    case 7:
                        if (jLY[6] == 0)
                        {
                            lumLY[6] = Convert.ToInt32(listaleiturasLY[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lumLY[6] = (lumLY[6] + Convert.ToInt32(listaleiturasLY[i].Sensor_luminosidade));
                        }
                        jLY[6]++;
                        break;
                    case 8:
                        if (jLY[7] == 0)
                        {
                            lumLY[7] = Convert.ToInt32(listaleiturasLY[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lumLY[7] = (lumLY[7] + Convert.ToInt32(listaleiturasLY[i].Sensor_luminosidade));
                        }
                        jLY[7]++;
                        break;
                    case 9:
                        if (jLY[8] == 0)
                        {
                            lumLY[8] = Convert.ToInt32(listaleiturasLY[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lumLY[8] = (lumLY[8] + Convert.ToInt32(listaleiturasLY[i].Sensor_luminosidade));
                        }
                        jLY[8]++;
                        break;
                    case 10:
                        if (jLY[9] == 0)
                        {
                            lumLY[9] = Convert.ToInt32(listaleiturasLY[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lumLY[9] = (lumLY[9] + Convert.ToInt32(listaleiturasLY[i].Sensor_luminosidade));
                        }
                        jLY[9]++;
                        break;
                    case 11:
                        if (jLY[10] == 0)
                        {
                            lumLY[10] = Convert.ToInt32(listaleiturasLY[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lumLY[10] = (lumLY[10] + Convert.ToInt32(listaleiturasLY[i].Sensor_luminosidade));
                        }
                        jLY[10]++;
                        break;
                    case 12:
                        if (jLY[11] == 0)
                        {
                            lumLY[11] = Convert.ToInt32(listaleiturasLY[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lumLY[11] = (lumLY[11] + Convert.ToInt32(listaleiturasLY[i].Sensor_luminosidade));
                        }
                        jLY[11]++;
                        break;
                    default:
                        break;
                }

            }
            for (int i = 0; i < 12; i++)
            {
                if (jLY[i] != 0)
                    lumLY[i] = lumLY[i] / jLY[i];
            }

            for (int i = 0; i < listaleiturasTY.Count(); i++)
            {
                switch (listaleiturasTY[i].Data.Month)
                {
                    case 1:

                        if (jTY[0] == 0)
                        {
                            lumTY[0] = Convert.ToInt32(listaleiturasTY[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lumTY[0] = (lumTY[0] + Convert.ToInt32(listaleiturasTY[i].Sensor_luminosidade));
                        }
                        jTY[0]++;
                        break;
                    case 2:
                        if (jTY[1] == 0)
                        {
                            lumTY[1] = Convert.ToInt32(listaleiturasTY[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lumTY[1] = (lumTY[1] + Convert.ToInt32(listaleiturasTY[i].Sensor_luminosidade));
                        }
                        jTY[1]++;
                        break;
                    case 3:
                        if (jTY[2] == 0)
                        {
                            lumTY[2] = Convert.ToInt32(listaleiturasTY[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lumTY[2] = (lumTY[2] + Convert.ToInt32(listaleiturasTY[i].Sensor_luminosidade));
                        }
                        jTY[2]++;
                        break;
                    case 4:
                        if (jTY[3] == 0)
                        {
                            lumTY[3] = Convert.ToInt32(listaleiturasTY[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lumTY[3] = (lumTY[3] + Convert.ToInt32(listaleiturasTY[i].Sensor_luminosidade));
                        }
                        jTY[3]++;
                        break;
                    case 5:
                        if (jTY[4] == 0)
                        {
                            lumTY[4] = Convert.ToInt32(listaleiturasTY[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lumTY[4] = (lumTY[4] + Convert.ToInt32(listaleiturasTY[i].Sensor_luminosidade));
                        }
                        jTY[4]++;
                        break;
                    case 6:
                        if (jTY[5] == 0)
                        {
                            lumTY[5] = Convert.ToInt32(listaleiturasTY[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lumTY[5] = (lumTY[5] + Convert.ToInt32(listaleiturasTY[i].Sensor_luminosidade));
                        }
                        jTY[5]++;
                        break;
                    case 7:
                        if (jTY[6] == 0)
                        {
                            lumTY[6] = Convert.ToInt32(listaleiturasTY[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lumTY[6] = (lumTY[6] + Convert.ToInt32(listaleiturasTY[i].Sensor_luminosidade));
                        }
                        jTY[6]++;
                        break;
                    case 8:
                        if (jTY[7] == 0)
                        {
                            lumTY[7] = Convert.ToInt32(listaleiturasTY[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lumTY[7] = (lumTY[7] + Convert.ToInt32(listaleiturasTY[i].Sensor_luminosidade));
                        }
                        jTY[7]++;
                        break;
                    case 9:
                        if (jTY[8] == 0)
                        {
                            lumTY[8] = Convert.ToInt32(listaleiturasTY[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lumTY[8] = (lumTY[8] + Convert.ToInt32(listaleiturasTY[i].Sensor_luminosidade));
                        }
                        jTY[8]++;
                        break;
                    case 10:
                        if (jTY[9] == 0)
                        {
                            lumTY[9] = Convert.ToInt32(listaleiturasTY[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lumTY[9] = (lumTY[9] + Convert.ToInt32(listaleiturasTY[i].Sensor_luminosidade));
                        }
                        jTY[9]++;
                        break;
                    case 11:
                        if (jTY[10] == 0)
                        {
                            lumTY[10] = Convert.ToInt32(listaleiturasTY[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lumTY[10] = (lumTY[10] + Convert.ToInt32(listaleiturasTY[i].Sensor_luminosidade));
                        }
                        jTY[10]++;
                        break;
                    case 12:
                        if (jTY[11] == 0)
                        {
                            lumTY[11] = Convert.ToInt32(listaleiturasTY[i].Sensor_luminosidade);
                        }
                        else
                        {
                            lumTY[11] = (lumTY[11] + Convert.ToInt32(listaleiturasTY[i].Sensor_luminosidade));
                        }
                        jTY[11]++;
                        break;
                    default:
                        break;
                }

            }
            for (int i = 0; i < 12; i++)
            {
                if (jTY[i] != 0)
                    lumTY[i] = lumTY[i] / jTY[i];
            }

            Chart9.Series.Clear();
            Chart9.Titles.Add("Luminosidade");
            Chart9.Legends.Add("Luminosidade");


            Series series = Chart9.Series.Add("Ano 2018");
            series.ChartType = SeriesChartType.Spline;
            series.Points.AddXY("Janeiro", lumTY[0]);
            series.Points.AddXY("Fevereiro", lumTY[1]);
            series.Points.AddXY("Maarço", lumTY[2]);
            series.Points.AddXY("Abril", lumTY[3]);
            series.Points.AddXY("Maio", lumTY[4]);
            series.Points.AddXY("Junho", lumTY[5]);
            series.Points.AddXY("Julho", lumTY[6]);
            series.Points.AddXY("Agosto", lumTY[7]);
            series.Points.AddXY("Setembro", lumTY[8]);
            series.Points.AddXY("Outubro", lumTY[9]);
            series.Points.AddXY("Novembro", lumTY[10]);
            series.Points.AddXY("Dezembro", lumTY[11]);
            Series series1 = Chart9.Series.Add("Ano 2017");
            series1.ChartType = SeriesChartType.Spline;
            series1.Points.AddXY("Janeiro", lumLY[0]);
            series1.Points.AddXY("Fevereiro", lumLY[1]);
            series1.Points.AddXY("Maarço", lumLY[2]);
            series1.Points.AddXY("Abril", lumLY[3]);
            series1.Points.AddXY("Maio", lumLY[4]);
            series1.Points.AddXY("Junho", lumLY[5]);
            series1.Points.AddXY("Julho", lumLY[6]);
            series1.Points.AddXY("Agosto", lumLY[7]);
            series1.Points.AddXY("Setembro", lumLY[8]);
            series1.Points.AddXY("Outubro", lumLY[9]);
            series1.Points.AddXY("Novembro", lumLY[10]);
            series1.Points.AddXY("Dezembro", lumLY[11]);
        }
    }
}